﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CubeGen : MonoBehaviour {
    [Serializable]
    private struct Sphere {
        public Vector3 center;
        public float radius;
    }

    private struct Voxel {
        public float potential;
        public Vector3 pos;
        public GameObject go;
        
        public Voxel(Vector3 pos, float potential) {
            this.pos = pos;
            this.potential = potential;
            if (potential >= 2.58 && potential < 2.69) {
                go = GameObject.CreatePrimitive(PrimitiveType.Cube);
                go.transform.position = pos;
                go.transform.localScale = cubeSize * Vector3.one;
            }
            else go = null;
        }
    }

    [SerializeField] private Vector3 maxBound;
    [SerializeField] private Vector3 minBound;
    [SerializeField] private Sphere[] spheres;
    
    private static float cubeSize = 0.5f;

    private float _numX;
    private float _numY;
    private float _numZ;

    List<Voxel> _voxels = new List<Voxel>();
    
    private void Start() {
        BBox();
        
        _numX = (maxBound.x - minBound.x) / cubeSize;
        _numY = (maxBound.y - minBound.y) / cubeSize;
        _numZ = (maxBound.z - minBound.z) / cubeSize;
        
        for (float i = 0; i <= _numX; ++i)
        for (float j = 0; j <= _numY; ++j)
        for (float k = 0; k <= _numZ; ++k) {
            var x = minBound.x + i * cubeSize;
            var y = minBound.y + j * cubeSize;
            var z = minBound.z + k * cubeSize;
            float potential = 0;
//            if (spheres.Any(s => IsInBounds(new Vector3(x, y, z), s))) {
//                potential = 1;
//            }
            foreach (var sphere in spheres) {
                potential += GetPotential(new Vector3(x, y, z), sphere);
            }
            _voxels.Add(new Voxel(new Vector3(x, y, z), potential));
        }
    }

    private float GetPotential(Vector3 vector3, Sphere sphere) {
        var dist = Vector3.Distance(vector3, sphere.center);
        return 1 / Mathf.Exp(1 / dist);
    }

    private void BBox() {
        minBound = spheres.Select(s => s.center - s.radius * Vector3.one)
                         .Aggregate(
                             Vector3.positiveInfinity,
                             (vec1, vec2) =>
                                 new Vector3(Mathf.Min(vec1.x, vec2.x), Mathf.Min(vec1.y, vec2.y), Mathf.Min(vec1.z, vec2.z)));
        minBound -= cubeSize * Vector3.one;

        maxBound = spheres.Select(s => s.center + s.radius * Vector3.one)
                         .Aggregate(Vector3.negativeInfinity,
                             (vec1, vec2) =>
                                 new Vector3(Mathf.Max(vec1.x, vec2.x), Mathf.Max(vec1.y, vec2.y), Mathf.Max(vec1.z, vec2.z)));
    }

    private void Update() { }

    float squared(float v) {
        return v * v;
    }

    private bool IsInBounds(Vector3 leftMost, Sphere sphere) {
        float distSquared = squared(sphere.radius);
        Vector3 g = sphere.center;
        Vector3 rightMost = leftMost + cubeSize * Vector3.one;
        if (g.x < leftMost.x) distSquared -= squared(g.x - leftMost.x);
        else if (g.x > rightMost.x) distSquared -= squared(g.x - rightMost.x);
        if (g.y < leftMost.y) distSquared -= squared(g.y - leftMost.y);
        else if (g.y > rightMost.y) distSquared -= squared(g.y - rightMost.y);
        if (g.z < leftMost.z) distSquared -= squared(g.z - leftMost.z);
        else if (g.z > rightMost.z) distSquared -= squared(g.z - rightMost.z);
        return distSquared >= 0;
    }
}