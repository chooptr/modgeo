# Geometric modelling

This project contains some geometric modelling techniques.

## Mesh generation
### Cylinder generation
![](res/cylinder.gif)

### Sphere generation
![](res/sphere.gif)

## Mesh simplification

Buddha original | 0.03 cell size | 0.06 cell size
----------------|----------------|-------------
![](res/buddha.png) | ![](res/buddha_003.png) | ![](res/buddha_006.png)

## Voxels and implicit surfaces

![](res/implicit.png)

## Bezier curves
### Bezier curves and connection of two curves
![](res/bezier.gif)
