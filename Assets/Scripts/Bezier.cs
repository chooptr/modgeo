﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bezier : MonoBehaviour {

    public Transform k1, k2, k3, k4;
    public LineRenderer lineRenderer;

    public int numPoints = 50;
    
    private Vector3[] _positions;
    
    void Start() {
        _positions = new Vector3[numPoints];
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = numPoints;
        DrawCubicCurve();
    }

    void Update() {
        DrawCubicCurve();
    }

    private void DrawCubicCurve() {
        for (int i = 0; i < numPoints; ++i) {
            float t = (i + 1f) / numPoints;
            _positions[i] = CalculateCubicBezierPoint(t, k1.position, k2.position, k3.position, k4.position);
        } 
        lineRenderer.SetPositions(_positions);
    }

    private Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3) {
       //B(t) = (1-t)3P0 + 3(1 - t)2tP1 + 3(1 - t)t2P2 + t3P3
       var u = 1 - t;
       var tt = t * t;
       var uu = u * u;
       return uu * u * p0
              + 3 * uu * t * p1
              + 3 * u * tt * p2
              + tt * t * p3;
    }
}