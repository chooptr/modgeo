﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierFusion : MonoBehaviour {

    public Bezier b1;
    public Bezier b2;

    private Transform b1k3, b1k4, b2k1, b2k2;
    
    public void Fusionner() {
        var k3Position = b1.k3.position;
        var k4Position = b1.k4.position;
        float distEnd = Vector3.Distance(k4Position, k3Position);
        Vector3 direction = Vector3.Normalize(k4Position - k3Position);
        b2.k1.position = k4Position;
        b2.k2.position = k4Position + direction * distEnd;
    }

    void Start() {
        Fusionner();
        b1k3 = b1.k3;
        b1k4 = b1.k4;
        b2k1 = b2.k1;
        b2k2 = b2.k2;
    }

    void Update() {
        if (b1k3.hasChanged || b1k4.hasChanged || b2k1.hasChanged || b2k2.hasChanged) {
            Fusionner();
        }
    }
}